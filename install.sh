#!/bin/bash

# Set some variables
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Get root password
printf "%s" "Set MySQL root password to? "
read MYSQL_PASSWORD

# Temporarily turn off unattended upgrades
sudo EDITOR='sed -Ei "
    s|unattended-upgrades/enable_auto_updates=.+|unattended-upgrades/enable_auto_updates=\"no\"|
    "' dpkg-reconfigure -f editor unattended-upgrades

# Just make sure we're all up-to-date
printf "${GREEN}Making sure the installation is up-to-date${NC}\n"
sudo dpkg --configure -a
sudo apt update ; sudo apt upgrade -y

# handy stuff
printf "${GREEN}Installing some handy tools${NC}\n"
sudo apt install net-tools software-properties-common zip unzip curl -y

# NGINX
printf "${GREEN}Installing NGINX${NC}\n"
sudo add-apt-repository ppa:ondrej/nginx -y
sudo apt install nginx -y

# PHP
printf "${GREEN}Installing PHP${NC}\n"
sudo add-apt-repository ppa:ondrej/php -y
sudo apt install php7.4-common php7.4-mysqli php7.4-xml php7.4-apcu php7.4-zip php7.4-fpm php7.4-mbstring php7.4-memcached php7.4-curl php7.4-mysql php7.4-gd -y

# MySQL
printf "${GREEN}Installing MySQL${NC}\n"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_PASSWORD"
sudo apt-get install mysql-server mysql-client -y

# Set up MySQL environment file - makes it easier to run MySQL on the command line
printf "${GREEN}Setting up MySQL environment${NC}\n"
echo "[client]" > ~/.my.cnf
echo "user = \"root\"" >> ~/.my.cnf
echo "password = \"$MYSQL_PASSWORD\"" >> ~/.my.cnf
echo "host = \"localhost\"" >> ~/.my.cnf

# Node
printf "${GREEN}Installing Node${NC}\n"
sudo curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
printf "${GREEN}Setting up Node versions${NC}\n"
nvm install lts/dubnium
nvm alias node10 lts/dubnium
nvm install lts/erbium
nvm alias node12 lts/erbium
nvm install lts/gallium
nvm alias node16 lts/gallium
nvm alias default lts/dubnium

# Composer v1
printf "${GREEN}Installing Composer v1${NC}\n"
php -r "copy('https://getcomposer.org/composer-1.phar', 'composer.phar');"
chmod a+x composer.phar
sudo mv composer.phar /usr/local/bin/composer1

# Composer v2
printf "${GREEN}Installing Composer v2${NC}\n"
php -r "copy('https://getcomposer.org/composer.phar', 'composer.phar');"
chmod a+x composer.phar
sudo mv composer.phar /usr/local/bin/composer

# Valet
printf "${GREEN}Installing Valet${NC}\n"
sudo apt-get install network-manager libnss3-tools jq xsel -y
composer global require cpriego/valet-linux
~/.config/composer/vendor/cpriego/valet-linux/valet install

# WARP
printf "${GREEN}Installing WARP${NC}\n"
curl https://pkg.cloudflareclient.com/pubkey.gpg | sudo gpg --yes --dearmor --output /usr/share/keyrings/cloudflare-warp-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/cloudflare-warp-archive-keyring.gpg] https://pkg.cloudflareclient.com/ $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/cloudflare-client.list
sudo apt update
sudo apt install cloudflare-warp -y

# Tidying up
printf "${GREEN}Tidying up${NC}\n"
sudo dpkg --configure -a
sudo apt update ; sudo apt upgrade -y
sudo apt autoremove -y

# Turn back on unattended upgrades
sudo EDITOR='sed -Ei "
    s|unattended-upgrades/enable_auto_updates=.+|unattended-upgrades/enable_auto_updates=\"yes\"|
    "' dpkg-reconfigure -f editor unattended-upgrades

# Finished message
printf "${GREEN}Finished${NC}\n\n"
printf "CloudFlare has been installed but now needs setting up.\n"
printf "Please visit: https://developers.cloudflare.com/warp-client/get-started/linux/\n"
