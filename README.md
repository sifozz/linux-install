# linux-install

This repo contains the steps detailed on [Linux Desktop for PHP stuff](https://holidaycottages.sharepoint.com/sites/DocumentManagement/Development%20Wiki/Linux%20Desktop%20for%20PHP%20stuff.aspx) in a script that can be run to automate the setting up of a Linux desktop machine for PHP development.

## Getting started

Run from a new terminal:

> bash ./install.sh

Done