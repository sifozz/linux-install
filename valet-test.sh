#!/bin/bash

# create folder and test file
mkdir ~/Documents/phpinfo
cd ~/Documents/phpinfo
echo '<?php phpinfo()?>' >> index.php

# set up valet
valet link phpinfo
